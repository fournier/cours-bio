unset key
set term pdf
set output "volume.pdf"
plot[0:10] "impro.txt" u 1:4 with lines lt 1 lw 2 , "impro.txt" u 1:5 with lines lt 3 lw 2
set output "crescendo.pdf"
plot[0:10] "impro.txt" u 1:2 with steps lt 1 lw 2 , "impro.txt" u 1:3 with steps lt 3 lw 2

