mkdir build
cd build
cmake ../cmake/ -DCMAKE_PREFIX_PATH=/home/rfo/Transfert-local/Cora/EDSTAR/star-engine/local
make








# Star Diffusion

Implementation of the heating algorithm presented in the Roffiac 2015 Workshop.

## Prerequisites

The *Star-Diff* program relies on [CMake](http://www.cmake.org) to build. It
also depends on the
[RSys](https://gitlab.com/vaplv/rsys/),
[Star-3D](https://gitlab.com/meso-star/star-3d/),
[Star-MC](https://gitlab.com/meso-star/star-mc/) and
[Star-SP](https://gitlab.com/meso-star/star-sp/) libraries.

First ensure that CMake is installed on your system. Then install all the
*Star-Diff* dependencies (i.e. RSys, Star-3D, Star-MC and Star-SP). Actually,
these prerequisites are components of the
[Star-Engine](https://gitlab.com/meso-star/star-engine/); it is thus sufficient
to deploy the Star-Engine to ensure that the Star-Diff dependencies are
correctly installed.

## Quick start (Linux)

Create a directory in which the *Star-Diff* GNU Makefile is going to be
generated.

    ~ $ cd star-diff
    ~/star-diff $ mkdir build
    ~/star-diff $ cd build

Generate the GNU Makefile from the CMakeLists.txt file of the *Star-Diff*
project. Note that one have to list in the `CMAKE_PREFIX_PATH` variable the
directory in which the Star-Engine is installed (e.g. `~/star-engine/local/`)
and the directory that contains the Star-Engine CMake packages (e.g.
`~/star-engine/local/lib/cmake`). These directories are used by CMake as
search paths to find the Star-Diff prerequisites.

    ~/star-diff/build $ cmake ../cmake -DCMAKE_PREFIX_PATH="~/star-engine/local/;~/star-engine/local/lib/cmake"

Finally build and execute the `Star-Diff` executable.

    ~/star-diff/build $ make
    ~/stat-diff/build $ ./sdiff

The following error can occur on the execution of the `sdiff`
program.

    ./sdiff: error while loading shared library: libXXX: cannot open shared object: No such file or directory

This means that a library on which Star-Diff depends is not found on its
execution. To resolve this issue, append the directory of the Star-Engine
libraries to the `LD_LIBRARY_PATH` environment variable of your shell.

    ~/star-diff/build $ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/star-engine/local/lib/

## License

*Star-Diff* is Copyright (C) |Meso|Star> 2015 (<contact@meso-star.com>). It is a
free software released under the [OSI](http://opensource.org)-approved CeCILL
license. You are welcome to redistribute it under certain conditions; refer to
the COPYING files for details.

